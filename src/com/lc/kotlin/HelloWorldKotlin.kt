package com.lc.kotlin

import com.sun.media.sound.EmergencySoundbank.toFloat
import jdk.nashorn.internal.runtime.JSType.toDouble
import jdk.nashorn.internal.runtime.JSType.toLong
import java.util.ArrayList

fun main(args: Array<String>) {

    println("hello world LC kotlin")
    println("hello world LC kotlin1")
    println("hello world LC kotlin2")

    var intsun = 123
    println(intsun)


    fun doubleM(x: Int): Int {
        return 2 * x
    }


    println(doubleM(2))

    // java Code : List<String> person=new ArrayList<>();
    val person = ArrayList<String>()
    person.add("1")
    person.add("2")
    person.add("3")
    println(person)

    println("\n----------------------------------------------------------")
    print("循环输出：")
    for (i in 1..4) print(i) // 输出“1234”
    println("\n----------------------------------------------------------")
    print("设置步长：")
    for (i in 1..4 step 2) print(i) // 输出“13”
    println("\n----------------------------------------------------------")
    print("使用 downTo：")
    for (i in 4 downTo 1 step 2) print(i) // 输出“42”
    println("\n----------------------------------------------------------")
    print("使用 until：")
    // 使用 until 函数排除结束元素
    for (i in 1 until 4) {   // i in [1, 4) 排除了 4
        print(i)
    }

    println("\n----------------------------------------------------------")

//    类型转换
//    由于不同的表示方式，较小类型并不是较大类型的子类型，较小的类型不能隐式转换为较大的类型。 这意味着在不进行显式转换的情况下我们不能把 Byte 型值赋给一个 Int 变量。
//
//    val b: Byte = 1 // OK, 字面值是静态检测的
//    val i: Int = b // 错误
//    我们可以代用其toInt()方法。
//
//    val b: Byte = 1 // OK, 字面值是静态检测的
//    val i: Int = b.toInt() // OK
//    每种数据类型都有下面的这些方法，可以转化为其它的类型：
//
//    toByte(): Byte
//    toShort(): Short
//    toInt(): Int
//    toLong(): Long
//    toFloat(): Float
//    toDouble(): Double
//    toChar(): Char
//    有些情况下也是可以使用自动类型转化的，前提是可以根据上下文环境推断出正确的数据类型而且数学操作符会做相应的重载。例如下面是正确的：
//
//    val l = 1L + 3 // Long + Int => Long
//
//    位操作符
//    对于Int和Long类型，还有一系列的位操作符可以使用，分别是：
//
//    shl(bits) – 左移位 (Java’s <<)
//    shr(bits) – 右移位 (Java’s >>)
//    ushr(bits) – 无符号右移位 (Java’s >>>)
//    and(bits) – 与
//    or(bits) – 或
//    xor(bits) – 异或
//    inv() – 反向
//    字符
//    和 Java 不一样，Kotlin 中的 Char 不能直接和数字操作，Char 必需是单引号 ' 包含起来的。比如普通字符 '0'，'a'。
//
//    fun check(c: Char) {
//        if (c == 1) { // 错误：类型不兼容
//            // ……
//        }
//    }
//    字符字面值用单引号括起来: '1'。 特殊字符可以用反斜杠转义。 支持这几个转义序列：\t、 \b、\n、\r、\'、\"、\\ 和 \$。 编码其他字符要用 Unicode 转义序列语法：'\uFF00'。
//
//    我们可以显式把字符转换为 Int 数字：
//
//    fun decimalDigitValue(c: Char): Int {
//        if (c !in '0'..'9')
//            throw IllegalArgumentException("Out of range")
//        return c.toInt() - '0'.toInt() // 显式转换为数字
//    }
//    当需要可空引用时，像数字、字符会被装箱。装箱操作不会保留同一性。
//
//    布尔
//    布尔用 Boolean 类型表示，它有两个值：true 和 false。
//
//    若需要可空引用布尔会被装箱。
//
//    内置的布尔运算有：
//
//    || – 短路逻辑或
//    && – 短路逻辑与
//    ! - 逻辑非

    println("\n----------------------------------------------------------")

    println("比较两个数字")
    val aa: Int = 10000
    println(aa === aa) // true，值相等，对象地址相等

    //经过了装箱，创建了两个不同的对象
    val boxedA: Int? = aa
    val anotherBoxedA: Int? = aa

    //虽然经过了装箱，但是值是相等的，都是10000
    println(boxedA === anotherBoxedA) //  false，值相等，对象地址不一样
    println(boxedA == anotherBoxedA) // true，值相等

    println("\n----------------------------------------------------------")

    //[1,2,3]
    val a = arrayOf(1, 2, 3)
    //[0,2,4]
    val b = Array(3, { i -> (i * 2) })

    //读取数组内容
    println(a[0])    // 输出结果：1
    println(b[1])    // 输出结果：2

    println("\n----------------------------------------------------------")

    val text = """
    |多行字符串
    |LC
    |多行字符串
    |Kotlin
    """.trimMargin()
    println(text)    // 前置空格删除了

    println("\n----------------------------------------------------------")

    val i = 10
    val s = "i = $i" // 求值结果为 "i = 10"
    println(s)

    println("\n----------------------------------------------------------")

    val s1 = "LCLoveKotlin"
    val str = "$s1.length is ${s1.length}" // 求值结果为 "runoob.length is 6"
    println(str)

    println("\n----------------------------------------------------------")

    val price = """
    ${'$'}9.99
    """
    println(price)  // 求值结果为 $9.99

    println("\n----------------------------------------------------------")


}