package com.lc.kotlin

import com.alibaba.fastjson.JSON
import com.lc.kotlin.pojo.Student
import com.lc.kotlin.utils.*
import java.math.BigDecimal


/**
 * main方法
 */
fun main(args: Array<String>) {
    //--------------------------------------------------------------------------------
//    try {
//        var result: Int = divide(5, 0)
//        println(result)
//    } catch (e: Exception) {
//        println(" 捕获的异常信息为：" + e.message)
//    } finally {
//        println("finally")
//    }
//    println(" 程序继续执行")
//    println("Go")
//    println(Math.random())
//
//    for (i in 1..10) {
//        println(i)
//    }
//
//    var listtemp = listOf<String>("1", "2");
//    println(listtemp.size)
//    for (lt in listtemp) {
//        println(lt)
//    }

    //--------------------------------------------------------------------------------
//    var listtemp2 = mutableListOf<String>();
//    listtemp2.add("3");
//    listtemp2.add("4");
//    listtemp2.add("5");
//    listtemp2.add("6");
//    listtemp2.add("7");
//    println("$listtemp2 的长度为 ${listtemp2.size}");
//    for (lt2 in listtemp2) {
//        println("具体内容为: ${lt2} ")
//    }

    //--------------------------------------------------------------------------------
    //print_app
    print_app();

    //--------------------------------------------------------------------------------
    val list_filter = listOf(1, 2, 3, 4, 5, 6, 7) //声明一个不可变的List
    print_app(list_filter.filter { it % 3 != 0 })

    val listStu: MutableList<Student> = ArrayList()
    val student = Student("bob", 24)
    val student12 = Student("lily", 23)
    listStu.add(student)
    listStu.add(student12)
    print_app("*******javaBean  to jsonString*******")
    val str1 = JSON.toJSONString(student)
    print_app(str1)
    //直接调用JsonUtils的工具类
    print_app(JsonUtils.objectToJson(student))
    //--------------------------------------------------------------------------------
    val listStr: MutableList<String> = ArrayList()
    listStr.add("111")
    listStr.add("222")
    print_app(listStr)

    val listStr2: MutableList<String> = mutableListOf()
    listStr2.add("333")
    listStr2.add("444")
    print_app(listStr2)
    //--------------------------------------------------------------------------------
    var list: List<Int> = listOf<Int>()
    var set: Set<Int> = setOf<Int>()
    var map: Map<String, Int> = mapOf<String, Int>()

    var mutableList: MutableList<Int> = mutableListOf()
    mutableList.add(1)
    mutableList.add(2)
    mutableList.add(3)
    mutableList.add(4)
    mutableList.remove(1)
    mutableList.get(2)
    print_app(mutableList.get(2))
    mutableList.clear()

    var mutableSet: MutableSet<Int> = mutableSetOf()
    mutableSet.add(1)
    mutableSet.remove(1)
    mutableSet.contains(2)
    mutableSet.clear()

    var mutableMap: MutableMap<String, Int> = mutableMapOf()
    mutableMap.put("1", 1)
    mutableMap.remove("1")
    mutableMap.get("2")
    print_app(mutableMap.get("2"))
    mutableMap.clear()

// 可变和不可变集合转换
    val mList: List<Int> = listOf(1, 3, 5, 7, 9)
    val mMutableList = mList.toMutableList()
    //--------------------------------------------------------------------------------
    val b1 = BigDecimal("100.10")
    val b2 = BigDecimal("100.00")
    val b3 = BigDecimal("100.010")

    println(b1)
    println(b2)
    println(b3)

    println(rvZeroAndDotUtil.rvZeroAndDot(b1.toString()))
    println(rvZeroAndDotUtil.rvZeroAndDot(b2.toString()))
    println(rvZeroAndDotUtil.rvZeroAndDot(b3.toString()))
    //--------------------------------------------------------------------------------
    println(RMBfenyuanUtils.fenToYuan("612"))
    println(RMBfenyuanUtils.yuanToFen("1"))
    //--------------------------------------------------------------------------------
    println("**********:" + RandomChar.getChars(3, 5))
    System.out.println(Random16.RandomNum16());
    //--------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------
}


/**
 * 函数区
 */
fun divide(a: Int, b: Int): Int {
    var result: Int = a / b
    return result
}

/**
 * 打印方法
 */
fun print_app() {
    println("hello world.");
}

/**
 * 打印方法 传入什么 打印什么
 */
fun print_app(message: Any?) {
    println(message);
}