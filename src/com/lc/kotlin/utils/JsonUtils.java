package com.lc.kotlin.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lc.kotlin.pojo.Student;

import java.util.ArrayList;
import java.util.List;


/**
 * fastjson
 * <p>
 * Json详解以及fastjson使用教程_srj1095530512的博客-CSDN博客
 * https://blog.csdn.net/srj1095530512/article/details/82529759
 * <p>
 *
 * @date 2020年1月7日10:11:51
 */
public class JsonUtils {

    /**
     * 将对象转换成json字符串。
     * <p>Title: pojoToJson</p>
     * <p>Description: </p>
     *
     * @param data
     * @return
     */
    public static String objectToJson(Object data) {
        try {
            String string = JSON.toJSONString(data); //序列化
            return string;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将json结果集转化为对象
     *
     * @param jsonData json数据
     * @param clazz    对象中的object类型
     * @return
     */
    public static <T> T jsonToPojo(String jsonData, Class<T> beanType) {
        try {
            T t = JSON.parseObject(jsonData, beanType);
            return t;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {

        List<Student> list = new ArrayList<>();
        Student student = new Student("bob", 24);
        Student student12 = new Student("lily", 23);
        list.add(student);
        list.add(student12);
        System.out.println("*******javaBean  to jsonString*******");
        String str1 = JSON.toJSONString(student);
        System.out.println(str1);
        System.out.println(JSON.toJSONString(list));
        System.out.println();

        System.out.println("******jsonString to javaBean*******");
        //Student stu1=JSON.parseObject(str1,new TypeReference<Student>(){});
        Student stu1 = JSON.parseObject(str1, Student.class);
        System.out.println(stu1);
        System.out.println();

        System.out.println("******javaBean to jsonObject******");
        JSONObject jsonObject1 = (JSONObject) JSON.toJSON(student);
        System.out.println(jsonObject1.getString("name"));
        System.out.println();

        System.out.println("******jsonObject to javaBean******");
        Student student2 = JSON.toJavaObject(jsonObject1, Student.class);
        System.out.println(student2);
        System.out.println();

        System.out.println("*******javaBean to jsonArray******");
        List<Student> stulist = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            stulist.add(new Student("student" + i, i));

        }
        JSONArray jsonArrays = (JSONArray) JSON.toJSON(stulist);
        for (int i = 0; i < jsonArrays.size(); i++) {
            System.out.println(jsonArrays.getJSONObject(i));
        }
        System.out.println();

        System.out.println("*****jsonArry to javalist******");
        List<Student> myList = new ArrayList<>();
        for (int i = 0; i < jsonArrays.size(); i++) {

            Student student3 = JSON.toJavaObject(jsonArrays.getJSONObject(i), Student.class);
            myList.add(student3);
        }
        for (Student stu : myList) {
            System.out.println(stu);
        }

        System.out.println();

        System.out.println("*****jsonObject to jsonString*****");
        String str4 = JSON.toJSONString(jsonObject1);
        System.out.println(str4);
        System.out.println();

        System.out.println("*******jsonString to jsonObject*****");
        JSONObject jso1 = JSON.parseObject(str1);
        System.out.println(jso1.getString("name"));
        System.out.println();

        System.out.println("*****jsonString to jsonArray*****");
        JSONArray jArray = JSON.parseArray(JSON.toJSONString(stulist));
        for (int i = 0; i < jArray.size(); i++) {
            System.out.println(jArray.getJSONObject(i));
        }
        System.out.println();
    }
}
