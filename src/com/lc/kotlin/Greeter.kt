package com.lc.kotlin

/**

Greeter.kt
面向对象
Version: 1.0
Author: LC
DateTime:2019年6月3日09:35:50
UpdateTime:
一加壹博客最Top-一起共创1+1>2的力量！~LC
LC博客url: http://oneplusone.top/index.html
LC博客url: http://oneplusone.vip/index.html
一加壹.SNS LC - 又一个SNS社区: http://sns.oneplusone.vip
赞助一加壹博客最Top-LC万能收款码支持-支付宝、微信、QQ
http://lc.oneplusone.vip/donateMeByLC.html

 */

class Greeter(val name: String) {
    fun greet() {
        println("Greeter to LC Hello, $name")
    }
}

fun main(args: Array<String>) {
    Greeter("World!").greet()          // 创建一个对象不用 new 关键字
}