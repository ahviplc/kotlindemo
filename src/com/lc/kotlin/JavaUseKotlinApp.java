package com.lc.kotlin;

import com.lc.kotlin.pojo.Student;
import com.lc.kotlin.utils.KotlinUtilsKt;

public class JavaUseKotlinApp {
    /**
     * main方法
     *
     * @param args
     */
    public static void main(String[] args) {
        //KotlinUtils ku = new KotlinUtils();
        System.out.println(KotlinUtilsKt.test("我在测试JavaUseKotlinApp啊"));
        System.out.println(KotlinUtilsKt.getStrTemp());
        KotlinUtilsKt.setStrTemp("我是改变之后的String");
        System.out.println(KotlinUtilsKt.getStrTemp());

        Student stu = new Student();
        stu.setAge(18);
        //stu = null;//把此放开就是为null的情况
        if (stu != null && !stu.equals("")) {
            System.out.println(1);
            System.out.println(stu.equals(""));//false
        } else {
            System.out.println("stu.equals(\"\")" + " 这样写会报错.");
        }

    }
}
